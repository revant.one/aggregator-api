export interface MessageData {
  message?: string;
  clientId: string;
}
